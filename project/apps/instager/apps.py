from django.apps import AppConfig


class InstagerConfig(AppConfig):
    name = 'instager'
