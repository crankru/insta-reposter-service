# Generated by Django 2.1.3 on 2018-11-26 09:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('instager', '0010_option_text_template'),
    ]

    operations = [
        migrations.AddField(
            model_name='account',
            name='post_datetime',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='option',
            name='tags_suggest',
            field=models.TextField(blank=True, null=True),
        ),
    ]
