# Generated by Django 2.1.3 on 2018-11-21 15:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('instager', '0002_itemfile_url'),
    ]

    operations = [
        migrations.AddField(
            model_name='option',
            name='geo_lat',
            field=models.FloatField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='option',
            name='geo_lng',
            field=models.FloatField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='item',
            name='status',
            field=models.CharField(choices=[('new', 'new'), ('good', 'like'), ('bad', 'unlike')], db_index=True, default='new', max_length=10),
        ),
    ]
