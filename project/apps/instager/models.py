from django.db import models
from django.conf import settings
from django.contrib.postgres.fields import JSONField
from django.utils.html import mark_safe
from django.core.files import File
from django.core.files.temp import NamedTemporaryFile
from django.utils import timezone

# from instagram.client import InstagramAPI
from InstagramAPI import InstagramAPI
from sorl.thumbnail import get_thumbnail
from .api_wrapper import ApiWrapper
from datetime import datetime
import json
import requests
import warnings
import os
from math import sin, cos, sqrt, atan2, radians

class Account(models.Model):
    name = models.CharField(max_length=50)
    active = models.BooleanField(default=True)
    token = models.CharField(max_length=50)
    secret = models.CharField(max_length=50)
    login = models.CharField(max_length=50, blank=True)
    password = models.CharField(max_length=50, blank=True)
    post_datetime = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.name

    def get_api(self):
        return ApiWrapper(self, debug=settings.DEBUG)


class Option(models.Model):
    name = models.CharField(max_length=50)
    active = models.BooleanField(default=True)
    account = models.ForeignKey('Account', on_delete=models.CASCADE)
    tags = models.TextField()
    geo_lat = models.FloatField(blank=True, null=True)
    geo_lng = models.FloatField(blank=True, null=True)
    geo_radius = models.FloatField(blank=True, null=True)
    text_template_default = (
        'Amazing shot by @{username}\n'
        '{original_text}\n'
        '#{tags}'
    )
    text_template = models.TextField(default=text_template_default, blank=True, null=True)
    tags_suggest = models.TextField(blank=True, null=True)

    def __str__(self):
        return '{}: {}'.format(self.account, self.name)

    def get_tags_list(self):
        return self.tags.strip().split()

    def get_tags_suggest_list(self):
        return [tag.strip() for tag in self.tags_suggest.split('\n')]

    def save(self, *args, **kwargs):
        # remove spaces from tags and format it
        tags_list = [tag.replace(' ', '').replace('#', '').strip() for tag in self.tags.replace(',', '\n').split('\n')]
        self.tags = '\n'.join(tags_list)
        super(Option, self).save(*args, **kwargs)

    def is_geo_set(self):
        return self.geo_lat and self.geo_lng and self.geo_radius

class Item(models.Model):
    STATUS_NEW = 'new'
    STATUS_LIKE = 'good'
    STATUS_UNLIKE = 'bad'


    option = models.ForeignKey('Option', on_delete=models.CASCADE)
    status = models.CharField(
        max_length=10, 
        choices=(
            (STATUS_NEW, 'new'),
            (STATUS_LIKE, 'like'),
            (STATUS_UNLIKE, 'dislike'),
        ), 
        default=STATUS_NEW,
        db_index=True
    )
    date_add = models.DateTimeField(auto_now_add=True)
    code = models.CharField(max_length=20, unique=True)
    text = models.TextField()
    comment_count = models.IntegerField(default=0, verbose_name='Comments')
    like_count = models.IntegerField(default=0, verbose_name='Likes')
    data = JSONField()
    distance = models.FloatField(blank=True, null=True)
    is_valid = models.BooleanField(default=False)

    def __str__(self):
        return str(self.id)

    def save_from_json(self, option, data):
        if data.get('code') and Item.objects.filter(code=data.get('code')).exists():
            warnings.warn('Item with such `code` already exist')
            return False
        
        if not data.get('code'):
            warnings.warn('Empty `code` in json')
            return False

        if not data.get('location'):
            print('Skip post without location {}'.format(data.get('code')))
            return False

        media_type = int(data.get('media_type'))

        if data.get('caption'):
            text = data['caption'].get('text', '')
        else:
            text = ''

        if media_type in [2]: # 2 video, 8 carousel
            print('Video not supported; media_type={}'.format(media_type))
            # print(data.keys())
            return None

        self.option = option
        self.code = data['code']
        self.text = text
        self.comment_count = data.get('comment_count', 0)
        self.like_count = data.get('like_count', 0)
        self.data = json.dumps(data)

        self.geo_update()
        self.save()
        self.save_files()

    def get_data(self):
        return json.loads(self.data)

    def geo_update(self):
        if self.option.is_geo_set():
            lat, lng = self.get_lat_and_lng()
            if lat and lng:
                self.distance = self.calculate_distance()
                self.is_valid = self.is_distance_valid()

                if not self.is_valid:
                    self.status = self.STATUS_UNLIKE

    def save_files(self):
        data = self.get_data()
        media_type = int(data.get('media_type'))
        if media_type == 1:
            # print('media_type 1 =', media_type)
            url = data['image_versions2']['candidates'][0]['url']
            f = ItemFile(item=self, url=url)
            f.save()
            # print('Create file: {}'.format(f))
        elif media_type == 8:
            # print(data.keys())
            # print('media_type 8 =', media_type)
            for item in data.get('carousel_media'):
                if int(item.get('media_type')) != 1:
                    continue

                url = item['image_versions2']['candidates'][0]['url']
                f = ItemFile(item=self, url=url)
                f.save()
        else:
            print('Error: no images in {}'.format(self))

    def get_thumb(self):
        image = ItemFile.objects.filter(item=self, item_type='image')[:1]

        if len(image) == 0:
            return None

        im = get_thumbnail(image[0].item_file, '100x100', crop='center', quality=80)
        return mark_safe('<a href="{}{}" target="_blank"><img src="{}"></a>'.format(settings.MEDIA_URL, image[0].item_file, im.url))

    def get_tags(self):
        text = self.get_text()
        tags = [word.strip() for word in text.split() if word.startswith('#')]
        return ', '.join(tags)

    def get_lat_and_lng(self):
        data = self.get_data()
        if data.get('location'):
            return data['location']['lat'], data['location']['lng']
        else:
            return None, None

    def calculate_distance(self):
        R = 6373.0 # approximate radius of earth in km
        lat1 = radians(self.option.geo_lat)
        lng1 = radians(self.option.geo_lng)
        lat2, lng2 = self.get_lat_and_lng()

        if lat1 and lng1 and lat2 and lng2:
            lat2, lng2 = radians(lat2), radians(lng2)
            dlng, dlat = lng2 - lng1, lat2 - lat1
            a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlng / 2)**2
            c = 2 * atan2(sqrt(a), sqrt(1 - a))
            distance = R * c
            return round(distance, 1)
        else:
            return None

    def is_distance_valid(self):
        distance = self.calculate_distance()

        if not distance:
            return False

        if distance <= self.option.geo_radius:
            return True
        else:
            return False

    def get_username(self):
        return self.get_data()['user']['username']

    def get_text(self, remove_tags=False):
        text = self.get_data()['caption'].get('text', '')
        if remove_tags:
            text = ' '.join([word for word in text.split() if not word.startswith('#')])
        return text


def file_dir(instance, filename):
    today = datetime.today()
    return 'accounts/{account_id}/{year}-{month}-{day}/{filename}'.format(
        account_id=instance.item.option.account.id,
        year=today.year,
        month=today.month,
        day=today.day,
        filename=filename
    )

class ItemFile(models.Model):
    item = models.ForeignKey('Item', on_delete=models.CASCADE)
    item_type = models.CharField(max_length=10, choices=(
        ('image', 'image'),
        ('video', 'video')
    ), default='image')
    item_file = models.FileField(upload_to=file_dir)
    url = models.CharField(max_length=255, db_index=True)
    date_add = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.id)

    def get_ext(self):
        pass

    def get_file_type(self):
        if self.url:
            file_name = self.url.split('/')[-1].split('?')[0]
        else:
            file_name = None

        if file_name in ['mp4']:
            return 'video'
        else:
            return 'image'

    def _save_tmp_file(self):
        r = requests.get(self.url) # stream=True
        if r.status_code != 200:
            return None

        tmp_file = NamedTemporaryFile(delete=True)
        tmp_file.write(r.content)
        tmp_file.flush()
        file_name = self.url.split('/')[-1].split('?')[0]
        return tmp_file, file_name

    def save(self, *args, **kwargs):
        if not self.item_type:
            self.item_type = self.get_file_type()

        if not self.item_file:
            file_content, file_name = self._save_tmp_file()
            self.item_file.save(file_name, File(file_content))

        super(ItemFile, self).save(*args, **kwargs)

    def delete(self):
        self.item_file.delete()
        super(ItemFile, self).delete()

    def get_preview(self):
        im = get_thumbnail(self.item_file, '100x100', crop='center', quality=80)
        # return mark_safe('<a href="{}{}" target="_blank"><img src="{}"></a>'.format(settings.MEDIA_URL, image[0].item_file, im.url))
        return mark_safe('<img src="{}">'.format(im.url))

class Post(models.Model):
    item = models.ForeignKey('Item', on_delete=models.CASCADE)
    code = models.CharField(max_length=20, blank=True, null=True)
    images = models.ManyToManyField('ItemFile')
    text = models.TextField()
    post_after = models.DateTimeField(blank=True, null=True)
    is_posted = models.BooleanField(default=False)
    date_add = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.id)

    def publish(self):
        api = self.item.option.account.get_api()
        images = self.images.all()
        if len(images) > 1:
            images_list = []
            for i in images:
                images_list.append({
                    'type': 'photo',
                    'file': os.path.join(settings.MEDIA_ROOT, i.item_file.name),
                    # 'usertags': [{}]
                })
        else:
            images_list = os.path.join(settings.MEDIA_ROOT, images[0].item_file.name)

        res = api.uploadPhoto(images_list, self.text)
        if res and res.get('status') == 'ok':
            self.is_posted = True
            self.save()

            account = self.item.option.account
            account.post_datetime = timezone.now()
            account.save()
            print('Posting done')
        else:
            print('Error: {}'.format(res))