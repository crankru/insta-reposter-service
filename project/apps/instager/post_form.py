from django import forms
from django.conf import settings
from django.utils.html import format_html
from django.contrib.admin import widgets
# from django.forms.extras.widgets import SelectDateWidget

from project.apps.instager.models import Post, ItemFile

import os

class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ['images', 'text', 'post_after']

    def __init__(self, *args, **kwargs):
        super(PostForm, self).__init__(*args, **kwargs)
        item = kwargs['instance'].item
        item_files = ItemFile.objects.filter(item=item)
        file_choices = [(f.id, format_html(f.get_preview())) for f in item_files]
        self.fields['images'] = forms.MultipleChoiceField(
            choices=file_choices,
            widget=forms.CheckboxSelectMultiple,
        )
        self.fields['post_after'] = forms.SplitDateTimeField(widget=widgets.AdminSplitDateTime(), required=False)