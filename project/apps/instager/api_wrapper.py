from InstagramAPI import InstagramAPI
import warnings

class ApiWrapper():
    _api = None
    _is_logged_in = False

    def __init__(self, account, debug):
        if account.login and account.password:
            self._api = InstagramAPI(account.login, account.password, debug=debug)
        else:
            warnings.warn('Login or password for account "{}" is empty'.format(account))

    def auth(self):
        self._api.login()
        self._is_logged_in = self._api.isLoggedIn

    def get_response(self):
        return self._api.LastResponse

    def request(self, method, *args, **kwargs):
        # print(method, args, kwargs)
        # return None

        if not self._is_logged_in:
            self.auth()

        method_obj = getattr(self._api, method, None)
        if not method_obj:
            warnings.warn('Method "{}" does not exist'.format(method))
            return None
        elif not callable(method_obj):
            warnings.warn('Method "{}" does not callable'.format(method))
            return None

        try:
            res = method_obj(*args, **kwargs)
        except Exception as e:
            # warnings.warn(e)
            print(e)
            return None

        # ERROR: status_code != 200
        r = self.get_response()
        if r.status_code != 200:
            print(self.get_response())
            warnings.warn('Error: status_code={}, text={}'.format(
                self.get_response().status_code, self.get_response().text
            ))
            return None        
        # OK: status_code == 200
        else:
            return self._api.LastJson

    # ['results']
    def tag_suggest(self, tag):
        return self.request('searchTags', tag)

    # TODO 404 error when query two words
    def tag_search(self, tag):
        return self.request('tagFeed', tag)

    def searchLocation(self, query):
        return self.request('searchLocation', query)

    def getLocationFeed(self, locationId, maxid=''):
        return self.request('getLocationFeed', locationId, maxid)

    def uploadPhoto(self, photo_path, caption):
        if type(photo_path) == list:
            return self.request('uploadAlbum', photo_path, caption)
        else:
            return self.request('uploadPhoto', photo_path, caption)