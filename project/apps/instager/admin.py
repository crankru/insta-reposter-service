from django.contrib import admin, messages
from django.utils.html import format_html
from django.urls import reverse, path
from django.http import HttpResponseRedirect
from django.shortcuts import render
from .models import Account, Option, Item, ItemFile, Post

from django_object_actions import DjangoObjectActions, takes_instance_or_queryset

@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'active']
    list_display_links = ['id', 'name']

@admin.register(Option)
class OptionAdmin(DjangoObjectActions, admin.ModelAdmin):
    list_display = ['id', 'name', 'get_tags_list', 'active']
    list_display_links = ['name']
    change_actions = ['get_tags_suggest']

    def get_tags_suggest(self, request, obj):
        api = obj.account.get_api()
        tags_list = obj.get_tags_list()
        tags_suggest = ''
        for tag in tags_list:
            res = api.tag_suggest(tag)
            for t in res['results']:
                print(t) # media_count, search_result_subtitle (posts count)
                tags_suggest += '{} {}\n'.format(t['name'], t['media_count'])

        if(len(tags_suggest) > 0):
            obj.tags_suggest = tags_suggest
            obj.save()
            self.message_user(request, 'Tags suggest loaded', messages.INFO)
    get_tags_suggest.label = 'Load tags suggest'

@admin.register(Item)
class ItemAdmin(DjangoObjectActions, admin.ModelAdmin):
    list_display = ['id', 'option', 'get_thumb', 'status', 'comment_count', 
        'like_count', 'distance', 'is_valid', 'item_actions', 'date_add']
    list_filter = ['status', 'option']
    actions = ['like', 'unlike']
    change_actions = ['like', 'unlike', 'make_post']
    # changelist_actions = ['test_action']

    def changelist_view(self, request, extra_context=None):
        if not request.GET.get('status__exact'):
            q = request.GET.copy()
            q['status__exact'] = Item.STATUS_NEW
            request.GET = q
            request.META['QUERY_STRING'] = request.GET.urlencode()

        return super(ItemAdmin, self).changelist_view(request, extra_context=extra_context)

    def format_post_text(self, obj):
        return obj.option.text_template.format(
            username=obj.get_username(),
            original_text=obj.get_text(remove_tags=True),
            tags=' #'.join(obj.option.get_tags_suggest_list()),
        )

    def make_post(self, request, obj):
        from .post_form import PostForm
        if obj.option.text_template:
            text = self.format_post_text(obj)
        else:
            text = obj.get_text()

        new_post = Post(item=obj, text=text)
        opts = obj._meta

        if request.method == 'POST':
            form = PostForm(request.POST, instance=new_post)
            if form.is_valid():
                form.save()
                self.message_user(request, 'Post created', messages.INFO)
                return HttpResponseRedirect(reverse('admin:instager_item_changelist'))
        else:
            form = PostForm(instance=new_post)

        return render(request, 'admin/instager/post.html', {'item': obj, 'form': form})
        # return render(request, 'admin/change_form.html', {'item': obj, 'form': form, 'opts': opts}) # django template
    make_post.label = 'Make post'
    make_post.short_description = 'Test action short desc'

    @takes_instance_or_queryset
    def like(self, request, queryset):
        queryset.update(status=Item.STATUS_LIKE)
        self.message_user(request, 'Item liked')
        return self.action_redirect(request)
    like.short_description = 'Like'

    @takes_instance_or_queryset
    def unlike(self, request, queryset):
        queryset.update(status=Item.STATUS_UNLIKE)
        self.message_user(request, 'Item unliked')
        return self.action_redirect(request)
    unlike.short_description = 'Unlike'

    def action_redirect(self, request):
        if request.META.get('HTTP_REFERER'):
            url = request.META.get('HTTP_REFERER')
        else:
            url = reverse('admin:instager_item_changelist')
        return HttpResponseRedirect(url)

    def item_actions(self, obj):
        return format_html(
            '<a class="button" href="{}">+</a> <a class="button" href="{}">-</a>', 
            reverse('admin:instager_item_actions', kwargs={'pk': obj.pk, 'tool': 'like'}, current_app=self.admin_site.name),
            reverse('admin:instager_item_actions', kwargs={'pk': obj.pk, 'tool': 'unlike'}, current_app=self.admin_site.name)
        )
    item_actions.allow_tags = True

@admin.register(ItemFile)
class ItemFileAdmin(admin.ModelAdmin):
    list_display = ['id', 'item', 'item_type', 'item_file', 'date_add']

@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    raw_id_fields = ['images',]
    list_display = ['id', 'is_posted']
