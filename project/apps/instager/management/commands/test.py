from django.core.management.base import BaseCommand, CommandError
from django.core.files import File
from django.core.files.temp import NamedTemporaryFile
from project.apps.instager.models import Account, Option, Item

import instagram
import warnings
import json
import requests

class Command(BaseCommand):
    def handle(self, *args, **options):
        print('test command')

        items = Item.objects.all()
        for i in items:
            print(i.calculate_distance())