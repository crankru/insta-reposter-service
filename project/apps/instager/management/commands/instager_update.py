from django.core.management.base import BaseCommand, CommandError
from django.core.files import File
from django.core.files.temp import NamedTemporaryFile
from project.apps.instager.models import Account, Option, Item

import warnings
import json
import requests

class Command(BaseCommand):
    def handle(self, *args, **options):
        print('run update instager...')
        account_options_list = Option.objects.filter(active=True, account__active=True).order_by('account')

        for option in account_options_list:
            api = option.account.get_api()
            tags_list = option.get_tags_list()
            
            for tag in tags_list:
                # res = api.tag_suggest(tag)
                res = api.tag_search(tag)
                # res = json.loads(open('response.json', 'r').read())
                if not res:
                    continue

                # print(res.keys())
                # f = open('response.json', 'w')
                # f.write(api.get_response().text)
                for data in res['items']:
                    if data.get('media_type') in (1, 2, 8):
                        new_item = Item()
                        new_item.save_from_json(option, data)

                        if new_item.id:
                            print('Saved item {}'.format(data['code']))
                    else:
                        print('Error: unknown media_type={}'.format(data.get('media_type')))
                        print(data.keys())


                
