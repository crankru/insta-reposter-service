from django.core.management.base import BaseCommand, CommandError
from django.utils import timezone
from project.apps.instager.models import Post

# import instagram
import warnings
from datetime import timedelta

class Command(BaseCommand):
    def handle(self, *args, **options):
        print('Posting...')
        posts = Post.objects.filter(post_after__lte=timezone.now(), is_posted=False)
        for p in posts:
            if self.permission_to_post(p):
                print('Publish post {}'.format(p))
                p.publish()
            else:
                print('Time limit has not yet passed for account {}'.format(p.item.option.account))

    def permission_to_post(self, post):
        post_datetime = post.item.option.account.post_datetime
        tmp_datetime = timezone.now() - timedelta(hours=-1)
        print(post_datetime, tmp_datetime)
        if not post_datetime or post_datetime <= tmp_datetime:
            return True
        else:
            return False
        
        