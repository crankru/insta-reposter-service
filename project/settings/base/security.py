# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'dfg9d8f76gg6d89f7g698d7%^^&*6g98fsdfdsf'

# INTERNAL_IPS = ['172.19.0.1']
ALLOWED_HOSTS = ['127.0.0.1', 'localhost']

AUTH_PASSWORD_VALIDATORS = [
    {'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator'},
    {'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator'},
    {'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator'},
    {'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator'}
]

X_FRAME_OPTIONS = 'DENY'
